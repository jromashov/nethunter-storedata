* (bitmold-dialog_rotation_fixes) Merge branch 'dialog_rotation_fixes' of https://github.com/bitmold/orbot into bitmold-dialog_rotation_fixes
* updating base versionCode to 1620200100 and changing how arch increments happen
* Fixes #329 Tor Wont Fully Stop With VPN Enabled
* Fixes #343 hosted services dialog crash
* Delete rs-rAR translation
* Delete rs-rAR translation
* MOAT Errors won't disappear on device rotation
* Limited MOAT Solution Length to spec's 20 byte limit
* Recent changes to the BridgeWizard would cause it not to display fully in landscape orientation. This was trivially fixed by adding a ScrollView
* When a new CAPTCHA is requested, immediately clear the old solution EditText instead of waiting for the CAPTCHA to finish downloading.
* About dialog remains open on rotation. Added an android:id to its ScrollView so its position is retained too.
